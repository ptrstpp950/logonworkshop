﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogonWorkshop.Models
{
    public class LoginModel
    {
        public string User { get; set; }
        public string Password { get; set; }
    }
}