﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LogonWorkshop.Models;

namespace LogonWorkshop.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(LoginModel model)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ProjectDB"].ConnectionString;

            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                var cmdCheckUser = new SqlCommand("SELECT Id FROM dbo.Users where Username=@user", conn);
                cmdCheckUser.Parameters.Add("user", SqlDbType.NVarChar);
                cmdCheckUser.Parameters["user"].Value = model.User;
                var userId = (int) (cmdCheckUser.ExecuteScalar() ?? -1);

                if (userId ==-1)
                {
                    return RedirectToAction("LoginFail");
                }

                var cmdCheckUserAndPassword = new SqlCommand("SELECT InvalidAttempts, LastBadLogin FROM dbo.Users where Id=@userId and Password=@pass", conn);
                cmdCheckUserAndPassword.Parameters.Add("userId", SqlDbType.Int);
                cmdCheckUserAndPassword.Parameters["userId"].Value = userId;
                cmdCheckUserAndPassword.Parameters.Add("pass", SqlDbType.NVarChar);
                cmdCheckUserAndPassword.Parameters["pass"].Value = model.Password;

                var badAttempt = 0;
                var lastBadLogin = DateTime.MaxValue;
                var successLogin = false;

                using (var reader = cmdCheckUserAndPassword.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        successLogin = true;
                        badAttempt = reader.GetInt32(0);
                        lastBadLogin = reader.GetDateTime(1);
                    }
                }


                if (!successLogin)
                {
                    var updateBadAttemptStr =
                        "UPDATE dbo.Users SET InvalidAttempts=InvalidAttempts+1, LastBadLogin = @date WHERE Id=@id";

                    var updateBadAttemptCmd = new SqlCommand(updateBadAttemptStr, conn);
                    updateBadAttemptCmd.Parameters.Add("id", SqlDbType.Int);
                    updateBadAttemptCmd.Parameters["id"].Value = userId;
                    updateBadAttemptCmd.Parameters.Add("date", SqlDbType.DateTime);
                    updateBadAttemptCmd.Parameters["date"].Value = DateTime.Now;
                    updateBadAttemptCmd.ExecuteNonQuery();
                
                    return RedirectToAction("LoginFail");
                }

                if (badAttempt >= 3 || (DateTime.Now - lastBadLogin).TotalMinutes < 1)
                {
                    return RedirectToAction("LoginBlocked");
                }
            }
            return RedirectToAction("LoginSuccess");
        }

        public ActionResult LoginBlocked()
        {
            return View();
        }

        public ActionResult LoginFail()
        {
            return View();
        }

        public ActionResult LoginSuccess()
        {
            return View();
        }

        public ActionResult Index2()
        {
            return View();
        }
    }
}